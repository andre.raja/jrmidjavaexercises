package exercises.checkcommonelement;

import java.util.Arrays;

public class CheckCommonElement {
    public boolean check(int[] ints, int[] ints1) {
        return Arrays.stream(ints)
                .anyMatch(element -> Arrays.stream(ints1)
                                .anyMatch(element1 -> element == element1));
//        for (int element : ints) {
//            for (int element1 : ints1) {
//                if (element == element1) {
//                    return true;
//                }
//            }
//        }
//        return false;
    }
}
