package exercises.checkcommonelement;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckCommonElementTest {

    private CheckCommonElement checkCommonElement = new CheckCommonElement();

    @Test
    void check() {
        int[] original = {1, 2, 3, 4, 5};
        int[] testTrue = {2, 6, 7};
        int[] testFalse = {6, 7, 8};

        assertTrue(checkCommonElement.check(original, testTrue));
        assertFalse(checkCommonElement.check(original, testFalse));
    }

}
