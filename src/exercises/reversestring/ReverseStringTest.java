package exercises.reversestring;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseStringTest {

    private ReverseString reverseString = new ReverseString();

    @Test
    void reverse() {
        String straight = "Dracula";
        String reversed = "alucarD";
        assertEquals(reversed, reverseString.reverse(straight));
    }

}
