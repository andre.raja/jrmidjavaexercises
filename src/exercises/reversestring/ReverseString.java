package exercises.reversestring;

public class ReverseString {
    public String reverse(String straight) {
        StringBuilder stringBuilder = new StringBuilder(straight).reverse();
        return stringBuilder.toString();
    }
}
