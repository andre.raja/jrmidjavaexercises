package exercises.evenNumbers;

import java.util.Arrays;

public class EvenNumbers {
    public int count(int[] ints) {
        return (int) Arrays.stream(ints)
                .filter(e -> e % 2 == 0)
                .count();
    }
}
