package exercises.evenNumbers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EvenNumbersTest {

    private EvenNumbers evenNumbers = new EvenNumbers();

    @Test
    void count() {
        int[] ints = {0, 5, 7, 9, 8, 3, 10, -2};
        assertEquals(4, evenNumbers.count(ints));
    }

}
