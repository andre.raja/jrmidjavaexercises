package exercises.pangram;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PangramTest {

    private Pangram pangram = new Pangram();

    @Test
    void isPangram() {
        String pangram1 = "The quick brown fox jumps over the lazy dog";
        String pangram2 = "Pack my box with five dozen liquor jugs";
        String notPangram = "Hello world";
        assertTrue(pangram.isPangram(pangram1));
        assertTrue(pangram.isPangram(pangram2));
        assertFalse(pangram.isPangram(notPangram));
        assertFalse(pangram.isPangram("    "));
    }

}
