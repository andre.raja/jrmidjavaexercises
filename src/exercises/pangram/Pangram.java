package exercises.pangram;

import java.util.HashSet;
import java.util.Set;

public class Pangram {
    public boolean isPangram(String str) {
        // Refactored
        if (str == null || str.trim().isEmpty()) return false;
        int numberOfLettersInAlphabet = 26;
        Set<Character> chars = new HashSet<>();
        for (Character ch : str.toLowerCase().toCharArray()) {
            if (Character.isLetter(ch)) chars.add(ch);
        }
        return chars.size() == numberOfLettersInAlphabet;

//        String[] alphabet = {
//                "a", "b", "c", "d", "e",
//                "f", "g", "h", "i", "j",
//                "k", "l", "m", "n", "o",
//                "p", "q", "r", "s", "t",
//                "u", "v", "w", "x", "y", "z"};
//        for (String letter : alphabet) {
//            if (!str.toLowerCase().contains(letter)) return false;
//        }
//        return true;
    }
}
