package exercises.arrayascendingorder;

import java.util.Arrays;

public class SortArray {
    public int[] sortArrayAsc(int[] toSort) {
        return Arrays.stream(toSort)
                .sorted()
                .toArray();
    }
}
