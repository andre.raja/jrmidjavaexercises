package exercises.arrayascendingorder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortArrayTest {

    private SortArray sortArray = new SortArray();

    @Test
    void sortArrayAsc() {
        int[] toSort = {78, 34, 100, -67, 1};
        int[] expected = {-67, 1, 34, 78, 100};
        assertArrayEquals(expected, sortArray.sortArrayAsc(toSort));
    }

}
