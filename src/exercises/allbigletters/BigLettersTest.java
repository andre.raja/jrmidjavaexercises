package exercises.allbigletters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BigLettersTest {

    private BigLetters bigLetters = new BigLetters();

    @Test
    void allBig() {
       assertTrue(bigLetters.isAllUppercase("SHOULD RETURN TRUE"));
       assertFalse(bigLetters.isAllUppercase("SHOULD RETURN false"));
       assertFalse(bigLetters.isAllUppercase(""));
       assertFalse(bigLetters.isAllUppercase("     "));
       assertFalse(bigLetters.isAllUppercase("F4LSE"));
    }

}
