package exercises.allbigletters;

import java.util.Objects;

public class BigLetters {
    public boolean isAllUppercase(String str) {
        // check null
        if (Objects.isNull(str) || str.trim().isEmpty()) return false;

        // check numbers
        // if (!str.replaceFirst("\\d", "").equals(str)) return false;

        // check character lowercase or digit
        for (char ch : str.toCharArray()) {
            if (Character.isDigit(ch) || Character.isLowerCase(ch)) return false;
        }
        return true;
    }
}
