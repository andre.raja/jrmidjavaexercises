package exercises.countcharacter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountCharacterTest {

    private CountCharacter countCharacter = new CountCharacter();

    @Test
    void count() {
        String original = "Any string to count selected digit";
        char toCount = 'g';
        int occurrences = 2;
        char toCount1 = 'a';
        int occurrences1 = 1;
        assertEquals(occurrences, countCharacter.count(original, toCount));
        assertEquals(occurrences1, countCharacter.count(original, toCount1));
    }

}
