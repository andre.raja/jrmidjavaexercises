package exercises.countcharacter;

public class CountCharacter {
    public int count(String original, char character) {
        return (int) original.toLowerCase()
                .chars()
                .filter(ch -> ch == character)
                .count();
    }
}
