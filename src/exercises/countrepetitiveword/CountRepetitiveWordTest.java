package exercises.countrepetitiveword;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountRepetitiveWordTest {

    private CountRepetitiveWord countRepetitiveWord = new CountRepetitiveWord();

    @Test
    void count() {
        String str = "Tomorrow is a new day of a new age that begins";
        String word = "new";
        int expected = 2;
        assertEquals(expected, countRepetitiveWord.count(str, word));
    }

}
