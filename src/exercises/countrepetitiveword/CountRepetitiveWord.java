package exercises.countrepetitiveword;

import java.util.Arrays;

public class CountRepetitiveWord {
    public int count(String str, String word) {
        return (int) Arrays.stream(str.toLowerCase().split(" "))
                .filter(w -> w.equals(word))
                .count();
    }
}
