package exercises.lowervalueinarray;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LowerValueTest {

    private LowerValue lowerValue = new LowerValue();

    @Test
    void find() {
        int[] ints = {0, 6, 8, -4, 0, 7, 1, -99, 100};
        assertEquals(-99, lowerValue.find(ints));
    }

}
