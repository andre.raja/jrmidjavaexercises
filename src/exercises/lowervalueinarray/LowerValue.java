package exercises.lowervalueinarray;

import java.util.Arrays;

public class LowerValue {
    public int find(int[] ints) {
        return Arrays.stream(ints).min().orElse(Integer.MIN_VALUE);
    }
}
