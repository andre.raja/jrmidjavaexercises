package exercises.removeblankspaces;

public class RemoveWhiteSpaces {
    public String remove(String str) {
//        return str.replaceAll(" +", "");
        // remembered regex to match white spaces
        return str.replaceAll("\\s+", "");
    }
}
