package exercises.removeblankspaces;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveWhiteSpacesTest {

    private RemoveWhiteSpaces removeWhiteSpaces = new RemoveWhiteSpaces();

    @Test
    void remove() {
        String original = " Thor   is  a    God ";
        String expected = "ThorisaGod";
        assertEquals(expected, removeWhiteSpaces.remove(original));
    }

}
