package exercises.arearraysequals;

import java.util.Arrays;

public class ArraysEquals {
    // Refactored
    public boolean checkEquality(int[] arr1, int[] arr2) {
        return Arrays.equals(arr1, arr2);
    }

//    public boolean checkEquality(int[] arr1, int[] arr2) {
//        if (arr1.length != arr2.length) return false;
//        for (int i = 0; i <= arr1.length -1; i++) {
//            if (arr1[i] != arr2[i]) return false;
//        }
//        return true;
//    }
}
