package exercises.arearraysequals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArraysEqualsTest {

    private ArraysEquals arraysEquals = new ArraysEquals();

    @Test
    void checkEquals() {
        int[] arr1 = {1, 6, 0, 4};
        int[] arr2 = {1, 6, 0, 4};
        int[] arr3 = {1, 6, 2, 4};
        assertTrue(arraysEquals.checkEquality(arr1, arr2));
        assertFalse(arraysEquals.checkEquality(arr1, arr3));
    }

}
