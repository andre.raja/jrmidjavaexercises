package exercises.perfectsquare;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PerfectSquareTest {

    private PerfectSquare perfectSquare = new PerfectSquare();

    @Test
    void isPerfect() {
        assertTrue(perfectSquare.isPerfect(9));
        assertTrue(perfectSquare.isPerfect(4));
        assertTrue(perfectSquare.isPerfect(25));
    }

}
