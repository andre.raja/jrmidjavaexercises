package exercises.perfectsquare;

public class PerfectSquare {
    public boolean isPerfect(int i) {
        // refactor
        int sqrt = (int) Math.sqrt(i);
        return sqrt * sqrt == i;

        // first idea
//        double sqrt = Math.sqrt(i);
//        StringBuilder sb = new StringBuilder().append(sqrt);
//        return sb.reverse().toString().startsWith("0");
    }
}
