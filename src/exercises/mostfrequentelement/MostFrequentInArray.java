package exercises.mostfrequentelement;

import java.util.HashMap;
import java.util.Map;

public class MostFrequentInArray {
    public int getMostFrequentElement(int[] ints) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer element : ints) {
            map.put(element, map.getOrDefault(element, 0) + 1);
        }

        Integer mostFrequentElement = 0;
        Integer maxFrequency = 0;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            Integer element = entry.getKey();
            Integer frequency = entry.getValue();

            if(frequency > maxFrequency) {
                mostFrequentElement = element;
                maxFrequency = frequency;
            }
        }
        return mostFrequentElement;
    }
}
