package exercises.mostfrequentelement;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MostFrequentInArrayTest {

    private MostFrequentInArray mostFrequentInArray = new MostFrequentInArray();

    @Test
    void getMostFrequentElement() {
        int[] testArray = {8, 10, 8, 9, 5, 3, 8};
        int expected = 8;
        assertEquals(expected, mostFrequentInArray.getMostFrequentElement(testArray));
    }

}
