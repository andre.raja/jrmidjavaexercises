package exercises.secondhighervalue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SecondHigherValueTest {

    private SecondHigherValue secondHigherValue = new SecondHigherValue();

    @Test
    void getSecondHigherValue() {
        int[] ints = {-4, 9, 0, 3, 10, 4, -5, 7, 23};
        int expected = 10;
        int[] ints1 = {9, 0, 45, 1, 7, 45, 46};
        int expected1 = 45;
        assertEquals(expected, secondHigherValue.getSecondHigherValue(ints));
        assertEquals(expected1, secondHigherValue.getSecondHigherValue(ints1));
    }

}
