package exercises.secondhighervalue;

public class SecondHigherValue {
    public int getSecondHigherValue(int[] ints) {
        int higher = 0;
        int second = 0;
        for (int el : ints) {
            if (el > higher) {
                second = higher;
                higher = el;
            }
        }
        return second;
    }
}
