package exercises.factorial;

public class Factorial {
    public int calculate(int i) {
        int result = 1;
        for (int j = i; j>1; j--) {
            result *= j;
        }
        return result;
    }
}
