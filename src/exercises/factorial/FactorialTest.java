package exercises.factorial;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {

    private Factorial factorial = new Factorial();

    @Test
    void calculate() {
        assertEquals(6, factorial.calculate(3));
        assertEquals(120, factorial.calculate(5));
        assertEquals(1, factorial.calculate(-3));
    }

}
