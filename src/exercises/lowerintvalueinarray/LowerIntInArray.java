package exercises.lowerintvalueinarray;

import java.util.Arrays;

public class LowerIntInArray {
    public int findLowerValue(int[] ints) {
        return Arrays.stream(ints).min().orElse(Integer.MIN_VALUE);
    }
}
