package exercises.lowerintvalueinarray;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LowerIntInArrayTest {

    private LowerIntInArray lowerIntInArray = new LowerIntInArray();

    @Test
    void findLowerValue() {
        int[] ints = {0, 7, -1, 4};
        int[] ints1 = {};
        assertEquals(-1, lowerIntInArray.findLowerValue(ints));
        assertEquals(Integer.MIN_VALUE, lowerIntInArray.findLowerValue(ints1));
    }

}
