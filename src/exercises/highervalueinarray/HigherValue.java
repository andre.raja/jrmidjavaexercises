package exercises.highervalueinarray;

import java.util.Arrays;

public class HigherValue {
    public int findHigherInt(int[] ints) {
        // return Arrays.stream(ints).reduce(0, Math::max);
        return Arrays.stream(ints).max().orElse(0);
    }

    public static void main(String... args) {
        int[] intArray = new int[5];
        intArray[0] = 10;
        intArray[1] = -7;
        intArray[2] = 1001;
        intArray[3] = 40;
        intArray[4] = -1000;

        HigherValue higherValue = new HigherValue();
        System.out.println(higherValue.findHigherInt(intArray));
    }
}
