package exercises.highervalueinarray;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HigherValueTest {

    private HigherValue higherValue = new HigherValue();

    @Test
    void findHigherValueInAnArray() {
        int[] intArray = new int[5];
        intArray[0] = 10;
        intArray[1] = -7;
        intArray[2] = 1001;
        intArray[3] = 40;
        intArray[4] = 1000;

        assertEquals(1001, higherValue.findHigherInt(intArray));
    }

}
