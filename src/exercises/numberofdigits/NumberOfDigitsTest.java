package exercises.numberofdigits;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfDigitsTest {

    private NumberOfDigits numberOfDigits = new NumberOfDigits();

    @Test
    void count() {
        assertEquals(4, numberOfDigits.count(-2023));
        assertEquals(1, numberOfDigits.count(0));
    }

}
