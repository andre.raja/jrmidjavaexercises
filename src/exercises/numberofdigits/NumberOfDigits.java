package exercises.numberofdigits;

public class NumberOfDigits {
    public int count(int i) {
        return Integer.toString(Math.abs(i)).length();
    }
}
