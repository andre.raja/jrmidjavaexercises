package exercises.maxvalue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxValueTest {

    private MaxValue maxValue = new MaxValue();

    @Test
    void getMinValue() {
        int[] ints = {0, 7, -1, 9, -5, 3};
        int expected = 9;
        assertEquals(expected, maxValue.getMaxValue(ints));
    }

}
