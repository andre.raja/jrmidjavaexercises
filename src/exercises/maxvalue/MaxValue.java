package exercises.maxvalue;

import java.util.Arrays;

public class MaxValue {
    public int getMaxValue(int[] ints) {
        return Arrays.stream(ints)
                .max()
                .orElse(Integer.MAX_VALUE);
    }
}
