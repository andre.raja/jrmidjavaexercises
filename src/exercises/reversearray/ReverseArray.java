package exercises.reversearray;

import java.util.Arrays;

public class ReverseArray {
    public int[] reverse(int[] original) {
        int length = original.length;
        int[] reversed = new int[length];
        for (int i = 0, j = length - 1; i < length; i++, j--) {
            reversed[j] = original[i];
        }
        return reversed;
    }
}
