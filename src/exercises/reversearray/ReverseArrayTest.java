package exercises.reversearray;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseArrayTest {

    private ReverseArray reverseArray = new ReverseArray();

    @Test
    void reverse() {
        int[] expected = {5, 4, 3, 2, 1, 0};
        int[] original = {0, 1, 2, 3, 4, 5};
        assertArrayEquals(expected, reverseArray.reverse(original));
    }

}
