package exercises.sumelements;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumElementsArrayTest {

    private SumElementsArray sumElementsArray = new SumElementsArray();

    @Test
    void sum() {
        int[] ints = {0, 1, 2, 3, 5, 8, 13};
        int expected = 32;
        assertEquals(expected, sumElementsArray.sum(ints));
    }

}
