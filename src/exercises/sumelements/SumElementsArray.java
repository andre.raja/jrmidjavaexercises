package exercises.sumelements;

import java.util.Arrays;

public class SumElementsArray {
    public int sum(int[] ints) {
        return Arrays.stream(ints).sum();
    }
}
