package exercises.averagevalue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AverageValueTest {

    private AverageValue averageValue = new AverageValue();

    @Test
    void getAverageValue() {
        int[] ints = {5, 9, 1, 20, 7};
        double expected = 8.4;
        int[] ints1 = {5, 9, 1, 20, 7, -50, 1};
        double expected1 = -1;
        assertEquals(expected, averageValue.getAverageValue(ints));
        assertEquals(expected1, averageValue.getAverageValue(ints1));
    }
}
