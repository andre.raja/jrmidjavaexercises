package exercises.averagevalue;

import java.util.Arrays;

public class AverageValue {
    public double getAverageValue(int[] ints) {
        return Arrays.stream(ints).average().orElse(Double.MIN_VALUE);
    }
}
