package exercises.numberfromstring;

public class NumberFromString {
    public boolean isValid(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
