package exercises.numberfromstring;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberFromStringTest {

    private NumberFromString numberFromString = new NumberFromString();

    @Test
    void isValid() {
        assertTrue(numberFromString.isValid("79"));
        assertFalse(numberFromString.isValid("10.0"));
        assertFalse(numberFromString.isValid("11,8"));
        assertFalse(numberFromString.isValid("1 y"));
    }

}
