package exercises.allletters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AllLettersTest {

    private AllLetters allLetters = new AllLetters();

    @Test
    void check() {
        String stringOnlyWithLetters = "ThisStringHasOnlyLettersAndNoNumbersNorSpaces";
        String stringWithSpaces = "This is a regular string with spaces between words";
        String stringWithDigits = "Th1sStringC0nt41n5Num3r5 ";
        assertTrue(allLetters.check(stringOnlyWithLetters));
        assertFalse(allLetters.check(stringWithSpaces));
        assertFalse(allLetters.check(stringWithDigits));
    }

}
