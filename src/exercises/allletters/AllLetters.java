package exercises.allletters;

public class AllLetters {
    public boolean check(String str) {
        for (char ch : str.toCharArray()) {
            if (!Character.isLetter(ch)) return false;
        }
        return true;
    }
}
