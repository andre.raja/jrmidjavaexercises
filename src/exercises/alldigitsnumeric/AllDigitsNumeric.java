package exercises.alldigitsnumeric;

import java.util.regex.Pattern;

public class AllDigitsNumeric {
    public boolean check(String str) {
        // solution suggested by AI when askes for a new way to solve
        return str.matches("\\d+");

        // solution after remember method "matches"
//        return str.matches("[0-9]+");

        // first solution
//        String replaced = str.replaceAll("[0-9]", "");
//        return replaced.length() == 0;
    }
}
