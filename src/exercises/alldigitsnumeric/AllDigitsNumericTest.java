package exercises.alldigitsnumeric;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AllDigitsNumericTest {

    private AllDigitsNumeric allDigitsNumeric = new AllDigitsNumeric();

    @Test
    void check() {
        assertTrue(allDigitsNumeric.check("1908734567"));
        assertFalse(allDigitsNumeric.check("bi6tg7ib5478v8"));
    }

}
