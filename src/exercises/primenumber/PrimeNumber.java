package exercises.primenumber;

public class PrimeNumber {
    public boolean isPrime(int number) {
        if (number <= 1) return false;

        int middle = number/2;
        for (int i = 2; i <= middle; i++) {
            if(number % i == 0) return false;
        }

        return true;
    }

    public static void main(String[] args) {
        PrimeNumber primeNumber = new PrimeNumber();
        System.out.println(primeNumber.isPrime(5));
    }
}
