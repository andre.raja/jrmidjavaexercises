package exercises.primenumber;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimeNumberTest {

    private PrimeNumber primeNumber = new PrimeNumber();

    @Test
    void testIfIsPrimeNumber() {
        assertTrue(primeNumber.isPrime(2));
        assertTrue(primeNumber.isPrime(3));
        assertTrue(primeNumber.isPrime(5));
        assertTrue(primeNumber.isPrime(7));
        assertTrue(primeNumber.isPrime(11));
        assertTrue(primeNumber.isPrime(17));

        assertFalse(primeNumber.isPrime(0));
        assertFalse(primeNumber.isPrime(1));
        assertFalse(primeNumber.isPrime(4));
        assertFalse(primeNumber.isPrime(6));
        assertFalse(primeNumber.isPrime(9));
        assertFalse(primeNumber.isPrime(10));
    }
}
