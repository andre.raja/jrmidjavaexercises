package exercises.ispalindrome;

public class CheckPalindrome {
    public boolean isPalindrome(String str) {
        String reversed = new StringBuilder(str).reverse().toString();
        return str.equals(reversed);
    }

    public static void main(String... args) {
        CheckPalindrome checkPalindrome = new CheckPalindrome();
        System.out.println(checkPalindrome.isPalindrome("Bomb") + ": is NOT a palindrome.");
        System.out.println(checkPalindrome.isPalindrome("level") + ": is a palindrome");
    }
}
