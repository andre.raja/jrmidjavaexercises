package exercises.ispalindrome;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckPalindromeTest {

    private CheckPalindrome checkPalindrome = new CheckPalindrome();

    @Test
    void isPalindrome() {
        assertTrue(checkPalindrome.isPalindrome("radar"));
        assertFalse(checkPalindrome.isPalindrome("abelha"));
    }

}
