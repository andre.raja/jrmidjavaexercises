package exercises.numberofvowels;

import java.util.Arrays;

public class NumberOfVowels {
    public int getNumberOfVowels(String str) {
        return (int)Arrays.stream(str.toLowerCase().split(""))
                //.filter(c -> (c.equals("a") || c.equals("e") || c.equals("i") || c.equals("o") || c.equals("u")))
                .filter("aeiou"::contains)
                .count();
    }
}
