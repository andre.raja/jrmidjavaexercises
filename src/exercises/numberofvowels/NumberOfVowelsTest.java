package exercises.numberofvowels;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfVowelsTest {

    private NumberOfVowels numberOfVowels = new NumberOfVowels();

    @Test
    void getNumberOfVowels() {
        String paralelepipedo = "paralelepipedo"; // 7
        assertEquals(7, numberOfVowels.getNumberOfVowels(paralelepipedo));
    }

}
