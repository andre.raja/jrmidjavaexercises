package exercises.anagram;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnagramTest {

    private Anagram anagram = new Anagram();

    @Test
    void isAnagram() {
        assertTrue(anagram.isAnagram("roma", "amor"));
    }

}
