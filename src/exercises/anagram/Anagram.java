package exercises.anagram;

import java.util.HashMap;
import java.util.Map;

public class Anagram {
    public boolean isAnagram(String str1, String str2) {
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();
        if (str1.length() != str2.length()) return false;

        Map<Character, Integer> frequency = new HashMap<>();

        for (char ch : str1.toCharArray()) {
            frequency.put(ch, frequency.getOrDefault(ch, 0) +1);
        }

        for (char ch : str2.toCharArray()) {
            if (!frequency.containsKey(ch)) return false;

            frequency.put(ch, frequency.get(ch) -1);
            if (frequency.get(ch) == 0) frequency.remove(ch);
        }

        return frequency.isEmpty();
    }
}
