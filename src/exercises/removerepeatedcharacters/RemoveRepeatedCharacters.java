package exercises.removerepeatedcharacters;

public class RemoveRepeatedCharacters {
    // IDE suggests me the commented code, but I didn't like it because
    // I think it would create unnecessary strings to the pool.
    public String reverse(String string) {
        String parsed = "";
        // StringBuilder parsed = new StringBuilder();
        for (int i = 0; i <= string.length() - 1; i++) {
            String ch = string.substring(i, i+1);
            if (!parsed.contains(ch)) parsed += (ch);
            // if (!parsed.toString().contains(ch)) parsed.append(ch);
        }
        return parsed.toString();
    }
}
