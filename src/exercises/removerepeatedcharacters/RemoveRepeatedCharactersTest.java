package exercises.removerepeatedcharacters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveRepeatedCharactersTest {

    private RemoveRepeatedCharacters removeRepeatedCharacters = new RemoveRepeatedCharacters();

    @Test
    void remove() {
        assertEquals("ban", removeRepeatedCharacters.reverse("banana"));
        assertEquals("mafgo", removeRepeatedCharacters.reverse("mafagafo"));
    }

}
